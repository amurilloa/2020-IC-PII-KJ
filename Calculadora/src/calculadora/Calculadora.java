/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;

/**
 *
 * @author ALLAN
 */
public class Calculadora {

    private double memoria;
    private double n1;
    private char op;

    public Calculadora() {
        op = ' ';
    }

    public void setMemoria(double memoria) {
        this.memoria = memoria;
    }

    public double getMemoria() {
        return memoria;
    }

    public void clear() {
        memoria = 0;
    }

    public void memoriaMenos(Double numero) {
        memoria -= numero;
    }

    public void memoriaMas(Double numero) {
        memoria += numero;
    }

    public double raizCuadrada(double d) {
        return Math.sqrt(d);
    }

    public double funcion(double x) {
        return 1 / x;
    }

    public double igual(double d) {
        switch (op) {
            case '+':
                return n1 + d;
            case '-':
                return n1 - d;
            case '*':
                return n1 * d;
            case '/':
                return n1 / d;
            default:
                return 0;
        }
    }

    public void setN1(double n1) {
        this.n1 = n1;
    }

    public void setOp(char op) {
        this.op = op;
    }

    public char getOp() {
        return op;
    }

}
