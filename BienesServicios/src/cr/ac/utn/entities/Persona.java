/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.utn.entities;

import java.util.LinkedList;

/**
 *
 * @author amurilloa
 */
public abstract class Persona {

    private int id;
    private String cedula;
    private String nombre;
    private String priApellido;
    private String segApellido;
    private char sexo;
    private String correo;
    private String direccion;
    private LinkedList<Telefono> telefonos;

}
