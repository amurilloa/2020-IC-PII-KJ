/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.utn.bo;

import cr.ac.utn.dao.AreaDAO;
import cr.ac.utn.entities.Area;
import java.util.LinkedList;

/**
 *
 * @author amurilloa
 */
public class AreaBO {
    
    public void guardar(Area area) {
        if (area == null) {
            throw new RuntimeException("Debe crear área");
        }
        if (area.getCodigo().isBlank()) {
            throw new RuntimeException("Código es requerido");
        }
        if (area.getNombre().isBlank()) {
            throw new RuntimeException("Nombre es requerido");
        }

        new AreaDAO().insertar(area);

    }

    public LinkedList<Area> cargar() {
        return new AreaDAO().cargar();
    }

}
