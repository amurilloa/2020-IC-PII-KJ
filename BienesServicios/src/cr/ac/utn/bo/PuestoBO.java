/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.utn.bo;

import cr.ac.utn.dao.PuestoDAO;
import cr.ac.utn.entities.Area;
import cr.ac.utn.entities.Puesto;
import java.util.LinkedList;

/**
 *
 * @author amurilloa
 */
public class PuestoBO {

    public boolean guardar(Puesto p) {
        if (p == null) {
            throw new RuntimeException("Debe crear puesto");
        }

        if (p.getArea() == null) {
            throw new RuntimeException("Debe seleccionar el área del puesto");
        }
        if (p.getCodigo().isBlank()) {
            throw new RuntimeException("Código es requerido");
        }
        if (p.getNombre().isBlank()) {
            throw new RuntimeException("Nombre es requerido");
        }
        if (p.getSalarioBase() <= 0) {
            throw new RuntimeException("Salario Base debe ser mayor a 0");
        }
        return new PuestoDAO().insertar(p);
    }

    public LinkedList<Puesto> cargar(Area area) {
        if (area == null) {
            throw new RuntimeException("Debe seleccionar un área");
        }

        return new PuestoDAO().cargar(area);

    }
}
