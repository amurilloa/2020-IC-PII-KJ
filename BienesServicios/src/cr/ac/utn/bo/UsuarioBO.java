/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.utn.bo;

import cr.ac.utn.dao.UsuarioDAO;
import cr.ac.utn.entities.Usuario;
import java.util.LinkedList;

/**
 *
 * @author amurilloa
 */
public class UsuarioBO {

    public boolean registrar(Usuario u, String repass) {
        
        if(u.getUsuario().isBlank()){
            throw new RuntimeException("El usuario es requerido");
        }
        
        if(u.getCorreo().isBlank()){
            throw new RuntimeException("El correo es requerido");
        }
        
        if(!u.getContrasena().equals(repass) || u.getContrasena().length()<8){
            throw new RuntimeException("Las contraseñas no coinciden o debe tener al menos 8 carateres");
        }
        
        //Encriptar la contraseña
        
        return new UsuarioDAO().insertar(u);
    }

    public Usuario login(Usuario u) {
        if(u.getUsuario().isBlank()){
            throw new RuntimeException("El usuario/correo es requerido");
        }
        
        if(u.getContrasena().isBlank()){
            throw new RuntimeException("Favor digitar una contraseña, debe tener al menos 8 carateres");
        }
        return new UsuarioDAO().login(u);
    }

    public LinkedList<Usuario> buscar(String filtro, boolean conf) {
        if(filtro.isBlank() && !conf){
            throw new RuntimeException("Debe filtrar por al menos 3 caracteres");
        }
        return new UsuarioDAO().seleccionar(filtro);
    }
}
