/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.utn.dao;

import cr.ac.utn.entities.Area;
import cr.ac.utn.entities.Puesto;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author amurilloa
 */
public class PuestoDAO {

    public boolean insertar(Puesto p) {

        try ( Connection con = Conexion.getConexion()) {
            String sql = "insert into bs.puestos(id_area, codigo, nombre, salario, activo) "
                    + " VALUES (?, ?, ?, ?, ?)";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setInt(1, p.getArea().getId());
            stm.setString(2, p.getCodigo());
            stm.setString(3, p.getNombre());
            stm.setDouble(4, p.getSalarioBase());
            stm.setBoolean(5, p.isActivo());
            return stm.executeUpdate() > 0;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            String txt = e.getMessage().contains("puestos_codigo_key")
                    ? "El código ya se encuentra en uso" : "Favor intente nuevamente";
            throw new RuntimeException(txt);
        }

    }

    public LinkedList<Puesto> cargar(Area area) {
        LinkedList<Puesto> puestos = new LinkedList<>();
        
        try ( Connection con = Conexion.getConexion()) {
            String sql = "select id, id_area, codigo, nombre, salario, activo from bs.puestos "
                    + " where id_area = ?";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setInt(1, area.getId());
            ResultSet rs = stm.executeQuery();
            
            while (rs.next()) {
                puestos.add(cargar(rs));
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
            throw new RuntimeException("Favor intente nuevamente");
        }
        return puestos;
    }
    
    private Puesto cargar(ResultSet rs) throws SQLException{
        Puesto p = new Puesto();
        p.setId(rs.getInt(1));
        p.setArea(new AreaDAO().cargarID(rs.getInt(2)));
        p.setCodigo(rs.getString(3));
        p.setNombre(rs.getString(4));
        p.setSalarioBase(rs.getDouble(5));
        p.setActivo(rs.getBoolean(6));
        return p;
    }
}

