/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.utn.dao;

import cr.ac.utn.entities.Area;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.LinkedList;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author amurilloa
 */
public class AreaDAO {

    public void insertar(Area area) {

        try ( Connection con = Conexion.getConexion()) {
            String sql = "insert into bs.areas(codigo, nombre) values(?,?)";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setString(1, area.getCodigo());
            stm.setString(2, area.getNombre());
            stm.execute();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            String txt = e.getMessage().contains("areas_codigo_key")
                    ? "El código ya se encuentra en uso" : "Favor intente nuevamente";
            throw new RuntimeException(txt);
        }

    }

    public LinkedList<Area> cargar() {
        LinkedList<Area> areas = new LinkedList<>();
        try ( Connection con = Conexion.getConexion()) {
            String sql = "select  id, codigo, nombre, activo from bs.areas";
            PreparedStatement stm = con.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                areas.add(cargar(rs));
            }

        } catch (Exception e) {
            throw new RuntimeException("Favor intente nuevamente");
        }
        return areas;
    }

    public Area cargar(ResultSet rs) throws SQLException {
        Area a = new Area();
        a.setId(rs.getInt(1));
        a.setCodigo(rs.getString(2));
        a.setNombre(rs.getString(3));
        a.setActivo(rs.getBoolean(4));
        return a;
    }

    public Area cargarID(int idArea) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "select  id, codigo, nombre, activo from bs.areas where id = ?";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setInt(1, idArea);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                return cargar(rs);
            }
            return null;
        } catch (Exception e) {
            throw new RuntimeException("Favor intente nuevamente");
        }
    }
}
