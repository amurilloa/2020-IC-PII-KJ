/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.utn.dao;

import cr.ac.utn.entities.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author amurilloa
 */
public class UsuarioDAO {

    public boolean insertar(Usuario u) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "insert into bs.usuarios(usuario,correo,contrasena) "
                    + "values(?,?,?)";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, u.getUsuario());
            ps.setString(2, u.getCorreo());
            ps.setString(3, u.getContrasena());
            return ps.executeUpdate() >= 1;

        } catch (Exception e) {
            String err = e.getMessage().contains("usuarios_usuario_key")
                    ? "Nombre de Usuario no disponible"
                    : e.getMessage().contains("usuarios_correo_key")
                    ? "Correo de Usuario no disponible"
                    : "Problemas de conexión con el servidor";

            throw new RuntimeException(err);
        }

    }

    public Usuario login(Usuario u) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "select id, usuario, correo, contrasena, activo "
                    + " from bs.usuarios where contrasena = ? and (correo = ? or usuario = ?);";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, u.getContrasena());
            ps.setString(2, u.getUsuario());
            ps.setString(3, u.getUsuario());

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                return cargar(rs);
            }

            return null;
        } catch (Exception e) {
            String err = "Problemas de conexión con el servidor";

            throw new RuntimeException(err);
        }
    }

    private Usuario cargar(ResultSet rs) throws SQLException {
        Usuario u = new Usuario();
        u.setId(rs.getInt(1));
        u.setUsuario(rs.getString(2));
        u.setCorreo(rs.getString(3));
        u.setContrasena(rs.getString(4));
        u.setActivo(rs.getBoolean(5));
        return u;
    }

    public LinkedList<Usuario> seleccionar(String filtro) {
        LinkedList<Usuario> usuarios = new LinkedList<>();

        try ( Connection con = Conexion.getConexion()) {
            String sql = "select id, usuario, correo, contrasena, activo from bs.usuarios "
                    + " where lower(usuario) like lower(?) or lower(correo) like lower(?) ";

            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, filtro+'%');
            ps.setString(2, filtro+'%');

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                usuarios.add(cargar(rs));
            }

        } catch (Exception e) {
            String err = "Problemas de conexión con el servidor";

            throw new RuntimeException(err);
        }

        return usuarios;

    }
}
