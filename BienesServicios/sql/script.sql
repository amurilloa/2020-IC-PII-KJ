-- create database progra_dos;
create schema bs;

create table bs.areas(
	id serial primary key,
	codigo text not null unique, 
	nombre text not null,
	activo boolean default true	
);

create table bs.puestos(
	id serial primary key,
	id_area integer not null, 
	codigo text not null unique, 
	nombre text not null,
	salario numeric default 0, 
	activo boolean default true, 
	foreign key (id_area) references bs.areas(id) --alternativa afuera --> alter table add constraint foreign key (id_area) references bs.area(id)
);

create table bs.usuarios(
	id serial primary key, 
	usuario text not null unique,
	correo text not null unique,
	contrasena text not null,
	activo boolean default true
)

insert into bs.areas(codigo, nombre) values('FIN','Financiero')
select * from bs.areas
select * from bs.puestos;
-- insert into bs.puestos(id_area,codigo,nombre, salario)  values(3, 'RHG','Gerente de Recursos Humanos',560000);
-- update bs.puestos set id_area = 1 where id=2
-- delete from bs.areas where id = 3

select * from bs.usuarios where contrasena = '12345678' and (correo = 'allanmual@gmail.com' or usuario = 'allanmual@gmail.com');
