/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rrhh.bo;

import java.time.LocalDate;
import java.time.Period;
import java.util.LinkedList;
import rrhh.dao.EmpleadoDAO;
import rrhh.dao.UsuarioDAO;
import rrhh.entities.Empleado;
import rrhh.entities.Solicitud;
import rrhh.entities.Usuario;

/**
 *
 * @author amurilloa
 */
public class EmpleadoBO {

    public void activar(Empleado e) {
        if (e == null) {
            throw new RuntimeException("Debe seleccionar un empleado");
        }

        if (e.isActivo()) {
            return;
        }

        e.setActivo(true);
        new EmpleadoDAO().editar(e);
    }

    public void desactivar(Empleado e) {
        if (e == null) {
            throw new RuntimeException("Debe seleccionar un empleado");
        }

        if (!e.isActivo()) {
            return;
        }

        e.setActivo(false);
        new EmpleadoDAO().editar(e);
    }

    public LinkedList<Empleado> buscar(String filtro, boolean conf) {
        if (filtro.isBlank() && !conf) {
            throw new RuntimeException("Debe filtrar por al menos 3 caracteres");
        }
        return new EmpleadoDAO().seleccionar(filtro);
    }

    public boolean registrar(Empleado e) {

        if (String.valueOf(e.getCedula()).length() != 9) {
            throw new RuntimeException("El cédula es requerida o formato incorrecto");
        }

        if (e.getNombre().isBlank()) {
            throw new RuntimeException("El nombre es requerido");
        }

        if (e.getApellidoUno().isBlank()) {
            throw new RuntimeException("El primer apellido es requerido");
        }

        if (e.getTelefono().isBlank()) {
            throw new RuntimeException("El teléfono es requerido");
        }

        if (e.getId() == 0) { //Solo cuando es registro  
            return new EmpleadoDAO().insertar(e);
        } else {
            return new EmpleadoDAO().editar(e);
        }
    }

    public Empleado cargarEmp(Usuario u) {
        if (u == null) {
            throw new RuntimeException("Debe iniciar sesión con el usuario requerido");
        }
        return new EmpleadoDAO().cargarEmp(u.getId());
    }

    public LinkedList<Solicitud> cargarSolicitudes(Empleado e) {
        if (e == null) {
            throw new RuntimeException("Favor especificar el empleado");
        }
        return new EmpleadoDAO().cargarSolicitudes(e);
    }

    public boolean registrarSol(Solicitud s) {
        if (s == null) {
            throw new RuntimeException("Solicitud es requerida");
        }
        if (s.getEmpleado() == null) {
            throw new RuntimeException("La solicitud debe tener un empleado asociado");
        }

        if (s.getMotivo().isBlank()) {
            throw new RuntimeException("El motivo es requerido");
        }

        if (s.getFechaInicio().isBefore(LocalDate.now())) {
            throw new RuntimeException("No puede solicitar vacaciones para días pasados");
        }

        if (s.getFechaFin().isBefore(s.getFechaInicio())) {
            throw new RuntimeException("Fecha de Fin debe ser mayor a la Inicial");
        }

        if (s.getId() > 0) {
            return new EmpleadoDAO().editarSolicitud(s);
        } else {
            return new EmpleadoDAO().insertarSolicitud(s);
        }
    }

    public LinkedList<Solicitud> cargarSolicitudes() {
        return new EmpleadoDAO().cargarSolicitudes(null);
    }

    public int calcDias(Empleado empleado) {
        LocalDate contrato = empleado.getContrato().getFecha();
        LocalDate hoy = LocalDate.now();
        Period periodo = Period.between(contrato, hoy);
        int a = periodo.getYears();
        int m = periodo.getMonths();
        int dias = a * 12 + (a > 0 ? m : 0);

        LinkedList<Solicitud> solicitudes = cargarSolicitudes(empleado);
        for (Solicitud solicitud : solicitudes) {
            if (solicitud.getEstado() == 'A') {
                dias -= solicitud.getDias();
            }
        }

        return dias;
    }

}
