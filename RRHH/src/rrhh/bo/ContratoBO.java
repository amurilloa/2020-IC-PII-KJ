/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rrhh.bo;

import rrhh.dao.ContratoDAO;
import rrhh.entities.Contrato;
import rrhh.entities.Empleado;

/**
 *
 * @author amurilloa
 */
public class ContratoBO {

    public boolean guardar(Contrato c, Empleado empleado) {
        if (empleado == null || empleado.getId() <= 0) {
            throw new RuntimeException("Favor seleccionar un empleado");
        }

        if (c == null) {
            throw new RuntimeException("Favor seleccionar crear un contrato");
        }
        if (c.getPuesto() == null) {
            throw new RuntimeException("Favor seleccionar un puesto");
        }
        if (c.getSalario() < c.getPuesto().getSalario()) {
            throw new RuntimeException("El salario del contrato no puede ser menor al salario del puesto " + c.getPuesto().getSalario());
        }

        return new ContratoDAO().insertar(c, empleado);

    }

}
