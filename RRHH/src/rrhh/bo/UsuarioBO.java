/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rrhh.bo;

import java.util.LinkedList;
import org.apache.commons.codec.digest.DigestUtils;
import rrhh.dao.UsuarioDAO;
import rrhh.entities.Empleado;
import rrhh.entities.Usuario;

/**
 *
 * @author amurilloa
 */
public class UsuarioBO {

    public boolean registrar(Usuario u, String repass) {

        if (u.getUsuario().isBlank()) {
            throw new RuntimeException("El usuario es requerido");
        }

        if (u.getCorreo().isBlank()) {
            throw new RuntimeException("El correo es requerido");
        }

        if (u.getId() == 0) { //Solo cuando es registro
            if (!u.getContrasena().equals(repass) || u.getContrasena().length() < 8) {
                throw new RuntimeException("Las contraseñas no coinciden o debe tener al menos 8 carateres");
            }
            //Encriptar la contraseña
            //http://commons.apache.org/proper/commons-codec/download_codec.cgi
            u.setContrasena(DigestUtils.md5Hex(u.getContrasena()));
            return new UsuarioDAO().insertar(u);
        } else {
            return new UsuarioDAO().editar(u);
        }

    }

    public boolean editar(Usuario u) {

        if (u.getUsuario().isBlank()) {
            throw new RuntimeException("El usuario es requerido");
        }

        if (u.getCorreo().isBlank()) {
            throw new RuntimeException("El correo es requerido");
        }

        //Encriptar la contraseña
        //http://commons.apache.org/proper/commons-codec/download_codec.cgi
        u.setContrasena(DigestUtils.md5Hex(u.getContrasena()));
        return new UsuarioDAO().editar(u);
    }

    public Usuario login(Usuario u) {
        if (u.getUsuario().isBlank()) {
            throw new RuntimeException("El usuario/correo es requerido");
        }

        if (u.getContrasena().isBlank()) {
            throw new RuntimeException("Favor digitar una contraseña, debe tener al menos 8 carateres");
        }

        u.setContrasena(DigestUtils.md5Hex(u.getContrasena()));

        return new UsuarioDAO().login(u);
    }

    public LinkedList<Usuario> buscar(String filtro, boolean conf) {
        if (filtro.isBlank() && !conf) {
            throw new RuntimeException("Debe filtrar por al menos 3 caracteres");
        }
        return new UsuarioDAO().seleccionar(filtro);
    }

    public void activar(Usuario u) {
        if (u == null) {
            throw new RuntimeException("Debe seleccionar un usuario");
        }

        if (u.isActivo()) {
            return;
        }

        new UsuarioDAO().editarEstado(u, true);
    }

    public void desactivar(Usuario u) {
        if (u == null) {
            throw new RuntimeException("Debe seleccionar un usuario");
        }

        if (!u.isActivo()) {
            return;
        }

        new UsuarioDAO().editarEstado(u, false);
    }

    public String generarPass() {
        char[] pass = new char[8];

        for (int i = 0; i < pass.length; i++) {
            int ra = (int) (Math.random() * 26) + 97;
            pass[i] = (char) ra;
        }

        for (int i = 0; i < 2; i++) {
            int ra = (int) (Math.random() * 10) + 48;
            int pos = (int) (Math.random() * pass.length);
            pass[pos] = (char) ra;
        }

        char[] sim = {'$', '%', '&', '+', '*'};
        while (true) {
            int pos = (int) (Math.random() * pass.length);
            if (Character.isLetter(pass[pos])) {
                int posSim = (int) (Math.random() * sim.length);
                pass[pos] = sim[posSim];
                break;
            }
        }

        while (true) {
            int pos = (int) (Math.random() * pass.length);
            if (Character.isLetter(pass[pos])) {
                pass[pos] = Character.toUpperCase(pass[pos]);
                break;
            }
        }
        return String.valueOf(pass);
    }

    public void asociarEmp(Empleado e) {
        if (e == null) {
            throw new RuntimeException("Debe seleccionar un empleado");
        }
        if (e.getUsuario() == null) {
            throw new RuntimeException("Debe seleccionar un usuario");
        }
        
        new UsuarioDAO().asociar(e);
        
    }
}
