/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rrhh.bo;

import java.util.LinkedList;
import rrhh.dao.AreaDAO;
import rrhh.entities.Area;
import rrhh.entities.Puesto;

/**
 *
 * @author amurilloa
 */
public class AreaBO {

    public LinkedList<Area> cargarAreas(boolean activas) {
        return new AreaDAO().selAreas(activas);
    }

    public LinkedList<Puesto> cargarPuestos(Area area, boolean activos) {
        return new AreaDAO().selPuestos(area, activos);
    }
    
}
