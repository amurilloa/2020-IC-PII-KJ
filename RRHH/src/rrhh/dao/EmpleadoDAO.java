/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rrhh.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Date;
import java.util.LinkedList;
import rrhh.entities.Empleado;
import rrhh.entities.Solicitud;

/**
 *
 * @author amurilloa
 */
public class EmpleadoDAO {

    public LinkedList<Empleado> seleccionar(String filtro) {
        LinkedList<Empleado> empleados = new LinkedList<>();

        try ( Connection con = Conexion.getConexion()) {
            String sql = "select id, cedula, nombre, apellido_uno, apellido_dos, "
                    + " telefono, genero, estado_civil, direccion, activo from rrhh.empleados "
                    + " where lower(cedula) like lower(?) or lower(apellido_uno) "
                    + " like lower(?) order by apellido_uno, nombre";

            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, filtro + '%');
            ps.setString(2, filtro + '%');

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                empleados.add(cargar(rs));
            }

        } catch (Exception e) {
            String err = "Problemas de conexión con el servidor";

            throw new RuntimeException(err);
        }

        return empleados;
    }

    private Empleado cargar(ResultSet rs) throws SQLException {
        Empleado e = new Empleado();
        e.setId(rs.getInt(1));
        e.setCedula(rs.getInt(2));
        e.setNombre(rs.getString(3));
        e.setApellidoUno(rs.getString(4));
        e.setApellidoDos(rs.getString(5));
        e.setTelefono(rs.getString(6));
        e.setGenero(rs.getString(7).charAt(0));
        e.setEstadoCivil(rs.getString(8).charAt(0));
        e.setDireccion(rs.getString(9));
        e.setActivo(rs.getBoolean(10));

        e.setContrato(new ContratoDAO().cargarEmp(e.getId()));
        e.setUsuario(new UsuarioDAO().cargarUsr(e.getId()));

        return e;
    }

    public boolean insertar(Empleado e) {

        try ( Connection con = Conexion.getConexion()) {
            String sql = "insert into rrhh.empleados(cedula, nombre, apellido_uno, apellido_dos, telefono, genero, estado_civil, direccion, activo) "
                    + " values (?, ?, ?, ?, ?, ?,?, ?, ?)";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, e.getCedula());
            ps.setString(2, e.getNombre());
            ps.setString(3, e.getApellidoUno());
            ps.setString(4, e.getApellidoDos());
            ps.setString(5, e.getTelefono());
            ps.setString(6, String.valueOf(e.getGenero()));
            ps.setString(7, String.valueOf(e.getEstadoCivil()));
            ps.setString(8, e.getDireccion());
            ps.setBoolean(9, e.isActivo());

            return ps.executeUpdate() >= 1;

        } catch (Exception ex) {
            String err = ex.getMessage().contains("empleados_cedula_key")
                    ? "Cédula previamente registrada"
                    : "Problemas de conexión con el servidor";
            throw new RuntimeException(err);
        }
    }

    public boolean editar(Empleado e) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "update rrhh.empleados set cedula = ?, nombre= ?, apellido_uno= ?, "
                    + " apellido_dos= ?, telefono= ?, genero= ?, estado_civil= ?, "
                    + " direccion= ?, activo= ? where id = ?";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, e.getCedula());
            ps.setString(2, e.getNombre());
            ps.setString(3, e.getApellidoUno());
            ps.setString(4, e.getApellidoDos());
            ps.setString(5, e.getTelefono());
            ps.setString(6, String.valueOf(e.getGenero()));
            ps.setString(7, String.valueOf(e.getEstadoCivil()));
            ps.setString(8, e.getDireccion());
            ps.setBoolean(9, e.isActivo());
            ps.setInt(10, e.getId());

            return ps.executeUpdate() > 0;
        } catch (Exception ex) {
            String err = ex.getMessage().contains("empleados_cedula_key")
                    ? "Cédula previamente registrada"
                    : "Problemas de conexión con el servidor";
            throw new RuntimeException(err);
        }
    }

    public Empleado cargarEmp(int idUsuario) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "select e.id, e.cedula, e.nombre, e.apellido_uno, e.apellido_dos, e.telefono, e.genero, "
                    + " e.estado_civil, e.direccion, e.activo from rrhh.empleados e "
                    + " inner join rrhh.usuarios u on e.id = u.id_empleado "
                    + " where u.id = ?";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, idUsuario);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                return cargar(rs);
            }

            return null;
        } catch (Exception e) {
            String err = "Problemas de conexión con el servidor";
            throw new RuntimeException(err);
        }
    }

    public Empleado cargarID(int id) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "select e.id, e.cedula, e.nombre, e.apellido_uno, e.apellido_dos, e.telefono, e.genero, "
                    + " e.estado_civil, e.direccion, e.activo from rrhh.empleados e "
                    + " where e.id = ?";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                return cargar(rs);
            }

            return null;
        } catch (Exception e) {
            String err = "Problemas de conexión con el servidor";
            throw new RuntimeException(err);
        }
    }

    public LinkedList<Solicitud> cargarSolicitudes(Empleado e) {
        LinkedList<Solicitud> solicitures = new LinkedList<>();

        try ( Connection con = Conexion.getConexion()) {
            String sql = "select id, id_empleado, motivo, fecha_ini, fecha_fin, "
                    + " dias, observacion, estado from rrhh.vacaciones where ";

            if (e != null) {
                sql += " id_empleado = ? order by fecha_ini ";
            } else {
                sql += " estado='S' order by fecha_ini ";
            }

            PreparedStatement ps = con.prepareStatement(sql);
            if (e != null) {
                ps.setInt(1, e.getId());
            }

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                solicitures.add(cargarSol(rs));
            }

        } catch (Exception ex) {
            String err = "Problemas de conexión con el servidor";
            throw new RuntimeException(err);
        }

        return solicitures;
    }

    private Solicitud cargarSol(ResultSet rs) throws SQLException {
        Solicitud s = new Solicitud();
        s.setId(rs.getInt(1));
        s.setEmpleado(cargarID(rs.getInt(2)));
        s.setMotivo(rs.getString(3));
        s.setFechaInicio(rs.getDate(4).toLocalDate());
        s.setFechaFin(rs.getDate(5).toLocalDate());
        s.setDias(rs.getInt(6));
        s.setObservacion(rs.getString(7));
        s.setEstado(rs.getString(8).charAt(0));
        return s;
    }

    public boolean editarSolicitud(Solicitud s) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "update rrhh.vacaciones set motivo=?, fecha_ini=?, "
                    + " fecha_fin=?, dias = ?, observacion=?, estado = ? where id = ?";
            PreparedStatement ps = con.prepareStatement(sql);

            ps.setString(1, s.getMotivo());
            ps.setDate(2, Date.valueOf(s.getFechaInicio()));
            ps.setDate(3, Date.valueOf(s.getFechaFin()));
            ps.setInt(4, s.getDias());
            ps.setString(5, s.getObservacion());
            ps.setString(6, String.valueOf(s.getEstado()));
            ps.setInt(7, s.getId());

            return ps.executeUpdate() > 0;
        } catch (Exception ex) {
            String err = "Problemas de conexión con el servidor";
            throw new RuntimeException(err);
        }
    }

    public boolean insertarSolicitud(Solicitud s) {
        try ( Connection con = Conexion.getConexion()) {

            String sql = "insert into rrhh.vacaciones(id_empleado, motivo, fecha_ini, fecha_fin, estado) values(?, ?, ?, ?, ?)";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, s.getEmpleado().getId());
            ps.setString(2, s.getMotivo());
            ps.setDate(3, Date.valueOf(s.getFechaInicio()));
            ps.setDate(4, Date.valueOf(s.getFechaFin()));
            ps.setString(5, String.valueOf(s.getEstado()));
            return ps.executeUpdate() >= 1;

        } catch (Exception ex) {
            String err = "Problemas de conexión con el servidor";
            throw new RuntimeException(err);
        }
    }

}
