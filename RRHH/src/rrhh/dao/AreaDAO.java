/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rrhh.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import rrhh.entities.Area;
import rrhh.entities.Puesto;

/**
 *
 * @author amurilloa
 */
public class AreaDAO {

    public LinkedList<Area> selAreas(boolean activas) {
        LinkedList<Area> areas = new LinkedList<>();

        try ( Connection con = Conexion.getConexion()) {
            String sql = "select id, codigo, nombre, activo from rrhh.areas ";

            if (activas) {
                sql += " where activo = true";
            }

            PreparedStatement ps = con.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                areas.add(cargarA(rs));
            }

        } catch (Exception e) {
            String err = "Problemas de conexión con el servidor";
            throw new RuntimeException(err);
        }

        return areas;
    }

    private Area cargarA(ResultSet rs) throws SQLException {
        Area a = new Area();
        a.setId(rs.getInt(1));
        a.setCodigo(rs.getString(2));
        a.setNombre(rs.getString(3));
        a.setActivo(rs.getBoolean(4));
        return a;
    }

    public LinkedList<Puesto> selPuestos(Area area, boolean activos) {
        LinkedList<Puesto> puestos = new LinkedList<>();

        try ( Connection con = Conexion.getConexion()) {
            String sql = "select id, codigo, nombre, salario, activo from rrhh.puestos where id_area = ? ";

            if (activos) {
                sql += " and activo = true ";
            }

            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, area.getId());
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                puestos.add(cargarP(rs));
            }

        } catch (Exception e) {
            e.printStackTrace();
            String err = "Problemas de conexión con el servidor";
            throw new RuntimeException(err);
        }

        return puestos;
    }

    private Puesto cargarP(ResultSet rs) throws SQLException {
        Puesto p = new Puesto();
        p.setId(rs.getInt(1));
        p.setCodigo(rs.getString(2));
        p.setNombre(rs.getString(3));
        p.setSalario(rs.getDouble(4));
        p.setActivo(rs.getBoolean(5));
        return p;
    }

    public Puesto cargarPuesto(int idPuesto) {
                try ( Connection con = Conexion.getConexion()) {
            String sql = "select id, codigo, nombre, salario, activo from rrhh.puestos where id = ? ";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, idPuesto);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                return cargarP(rs);
            }

            return null;
        } catch (Exception e) {
            String err = "Problemas de conexión con el servidor";
            throw new RuntimeException(err);
        }
    }

}
