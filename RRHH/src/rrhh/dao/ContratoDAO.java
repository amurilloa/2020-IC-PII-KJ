/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rrhh.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import rrhh.entities.Contrato;
import rrhh.entities.Empleado;
import rrhh.entities.Puesto;

/**
 *
 * @author amurilloa
 */
public class ContratoDAO {

    public boolean insertar(Contrato c, Empleado e) {

        try ( Connection con = Conexion.getConexion()) {
            String sql = "insert into rrhh.contratos(id_empleado, id_puesto, fecha, salario) values (?, ?, ?, ?)";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, e.getId());
            ps.setInt(2, c.getPuesto().getId());
            ps.setDate(3, Date.valueOf(c.getFecha()));
            ps.setDouble(4, c.getSalario());

            return ps.executeUpdate() >= 1;

        } catch (Exception ex) {
            String err = "Problemas de conexión con el servidor";
            throw new RuntimeException(err);
        }
    }

    public Contrato cargarEmp(int idEmpleado) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "select id, id_puesto, fecha, salario, activo "
                    + " from rrhh.contratos where id_empleado = ? order by fecha desc limit 1";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, idEmpleado);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                return cargar(rs);
            }

            return null;
        } catch (Exception e) {
            String err = "Problemas de conexión con el servidor";
            throw new RuntimeException(err);
        }
    }

    private Contrato cargar(ResultSet rs) throws SQLException {
        Contrato c = new Contrato();
        c.setId(rs.getInt(1));
        c.setPuesto(new AreaDAO().cargarPuesto(rs.getInt(2)));
        c.setFecha(rs.getDate(3).toLocalDate());
        c.setSalario(rs.getDouble(4));
        c.setActivo(rs.getBoolean(5));
        return c;

    }
}
