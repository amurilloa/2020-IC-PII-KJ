/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rrhh.dao;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.imageio.ImageIO;
import rrhh.entities.Foto;
import rrhh.entities.Usuario;

/**
 *
 * @author amurilloa
 */
public class FotoDAO {

    public void insertar(Usuario u) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "insert into rrhh.imagenes(id_usuario, imagen) values (?,?)";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, u.getId());

            BufferedImage bf = imageToBufferedImage(u.getFoto().getFoto());

            ByteArrayOutputStream os = new ByteArrayOutputStream();
            ImageIO.write(bf, "jpg", os);
            InputStream in = new ByteArrayInputStream(os.toByteArray());
            ps.setBinaryStream(2, in);
            ps.execute();

        } catch (Exception e) {
            String err = "Problemas de conexión con el servidor";
            throw new RuntimeException(err);
        }
    }

    public static BufferedImage imageToBufferedImage(Image im) {
        BufferedImage bi = new BufferedImage(im.getWidth(null), im.getHeight(null), BufferedImage.TYPE_INT_RGB);
        Graphics bg = bi.getGraphics();
        bg.drawImage(im, 0, 0, null);
        bg.dispose();
        return bi;
    }

    public Foto cargar(int idUsuario) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "select id, imagen from rrhh.imagenes where id_usuario = ?";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, idUsuario);

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return cargar(rs);
            }
            return null;
        } catch (Exception e) {
            String err = "Problemas de conexión con el servidor";
            throw new RuntimeException(err);
        }
    }

    private Foto cargar(ResultSet rs) throws SQLException, IOException {
        Foto f = new Foto();
        f.setId(rs.getInt(1));
        Image imgdb = null;
        InputStream fis = rs.getBinaryStream(2);
        imgdb = ImageIO.read(fis);
        f.setFoto(imgdb);
        return f;
    }

}
