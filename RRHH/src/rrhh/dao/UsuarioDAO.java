/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rrhh.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import rrhh.entities.Empleado;
import rrhh.entities.Foto;
import rrhh.entities.Usuario;

/**
 *
 * @author amurilloa
 */
public class UsuarioDAO {
    
    public boolean insertar(Usuario u) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "insert into rrhh.usuarios(usuario,correo,contrasena) "
                    + "values(?,?,?) returning id";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, u.getUsuario());
            ps.setString(2, u.getCorreo());
            ps.setString(3, u.getContrasena());
            ResultSet rs = ps.executeQuery();
            
            if (rs.next()) {
                int id = rs.getInt(1);
                u.setId(id);
                if (u.getFoto() != null) {
                    new FotoDAO().insertar(u);
                }
                return true;
            }
            return false;
            
        } catch (Exception e) {
            String err = e.getMessage().contains("usuarios_usuario_key")
                    ? "Nombre de Usuario no disponible"
                    : e.getMessage().contains("usuarios_correo_key")
                    ? "Correo de Usuario no disponible"
                    : "Problemas de conexión con el servidor";
            
            throw new RuntimeException(err);
        }
    }
    
    public Usuario login(Usuario u) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "select u.id, u.usuario, u.correo, u.contrasena, u.activo, "
                    + " (a.id=1) as admin from rrhh.usuarios u "
                    + " inner join rrhh.contratos c on u.id_empleado = c.id_empleado "
                    + " inner join rrhh.puestos p on c.id_puesto = p.id "
                    + " inner join rrhh.areas a on p.id_area = a.id "
                    + " where u.activo and u.contrasena = ? and (u.correo = ? or u.usuario = ?) "
                    + " order by c.fecha desc limit 1";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, u.getContrasena());
            ps.setString(2, u.getUsuario());
            ps.setString(3, u.getUsuario());
            
            ResultSet rs = ps.executeQuery();
            
            if (rs.next()) {
                return cargar(rs);
            }
            
            return null;
        } catch (Exception e) {
            String err = "Problemas de conexión con el servidor";
            
            throw new RuntimeException(err);
        }
    }
    
    private Usuario cargar(ResultSet rs) throws SQLException {
        Usuario u = new Usuario();
        u.setId(rs.getInt(1));
        u.setUsuario(rs.getString(2));
        u.setCorreo(rs.getString(3));
        u.setContrasena(rs.getString(4));
        u.setActivo(rs.getBoolean(5));
        if (rs.getMetaData().getColumnCount() > 5) {
            u.setAdmin(rs.getBoolean(6));
        }
//      u.setFoto(new FotoDAO().cargar(u.getId()));
        return u;
    }
    
    public LinkedList<Usuario> seleccionar(String filtro) {
        LinkedList<Usuario> usuarios = new LinkedList<>();
        
        try ( Connection con = Conexion.getConexion()) {
            String sql = "select id, usuario, correo, contrasena, activo from rrhh.usuarios "
                    + " where lower(usuario) like lower(?) or lower(correo) like lower(?) order by usuario";
            
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, filtro + '%');
            ps.setString(2, filtro + '%');
            
            ResultSet rs = ps.executeQuery();
            
            while (rs.next()) {
                usuarios.add(cargar(rs));
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            String err = "Problemas de conexión con el servidor";
            
            throw new RuntimeException(err);
        }
        
        return usuarios;
        
    }
    
    public void editarEstado(Usuario u, boolean estado) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "update rrhh.usuarios set activo = ? where id = ?";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setBoolean(1, estado);
            ps.setInt(2, u.getId());
            ps.execute();
        } catch (Exception e) {
            String err = "Problemas de conexión con el servidor";
            throw new RuntimeException(err);
        }
    }
    
    public boolean editar(Usuario u) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "update rrhh.usuarios set usuario = ?, correo = ?, contrasena = ? , activo = ? where id = ?";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, u.getUsuario());
            ps.setString(2, u.getCorreo());
            ps.setString(3, u.getContrasena());
            ps.setBoolean(4, u.isActivo());
            ps.setInt(5, u.getId());
            return ps.executeUpdate() > 0;
        } catch (Exception e) {
            String err = "Problemas de conexión con el servidor";
            throw new RuntimeException(err);
        }
    }
    
    public Usuario cargarUsr(int idEmpleado) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "select id, usuario, correo, contrasena, activo "
                    + " from rrhh.usuarios where id_empleado = ?";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, idEmpleado);
            
            ResultSet rs = ps.executeQuery();
            
            if (rs.next()) {
                return cargar(rs);
            }
            
            return null;
        } catch (Exception e) {
            String err = "Problemas de conexión con el servidor";
            throw new RuntimeException(err);
        }
    }
    
    public void asociar(Empleado e) {
        try ( Connection con = Conexion.getConexion()) {
            System.out.println(e);
            String sql = "update rrhh.usuarios set id_empleado = null where id_empleado = ?"; //Borrar la asignación anterior 
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, e.getId());
            ps.execute();
            sql = "update rrhh.usuarios set id_empleado = ? where id = ?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, e.getId());
            ps.setInt(2, e.getUsuario().getId());
            ps.execute();
        } catch (Exception ex) {
            String err = "Problemas de conexión con el servidor";
            throw new RuntimeException(err);
        }
    }
}
