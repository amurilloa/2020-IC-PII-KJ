/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rrhh.entities;

/**
 *
 * @author amurilloa
 */
public class Usuario {
    
    private int id; //Uso interno del desarrollador
    private String usuario;
    private String correo;
    private String contrasena; //Encriptar contraseña y no deberñiamos mostrarla 
    private boolean activo;
    private Foto foto;
    private boolean admin;

    public Usuario() {
        activo = true;
    }

    public Usuario(int id, String usuario, String correo, String contrasena, boolean activo) {
        this.id = id;
        this.usuario = usuario;
        this.correo = correo;
        this.contrasena = contrasena;
        this.activo = activo;
    }

    public Usuario(int id, String usuario, String correo, String contrasena, boolean activo, Foto foto) {
        this.id = id;
        this.usuario = usuario;
        this.correo = correo;
        this.contrasena = contrasena;
        this.activo = activo;
        this.foto = foto;
    }
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public Foto getFoto() {
        return foto;
    }

    public void setFoto(Foto foto) {
        this.foto = foto;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public boolean isAdmin() {
        return admin;
    }
    
    
    @Override
    public String toString() {
        return String.format("%s - %s %s", usuario, correo, (activo ? "✔" : "✘"));
    }
    
}
