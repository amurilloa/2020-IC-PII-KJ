/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rrhh.entities;

/**
 *
 * @author amurilloa
 */
public class Empleado {

    private int id;
    private int cedula;
    private String nombre;
    private String apellidoUno;
    private String apellidoDos;
    private String telefono;
    private char genero;
    private char estadoCivil;
    private String direccion;
    private boolean activo;
    private Contrato contrato; //Contrato vigente o actual, el que tenga fecha más reciente
    private Usuario usuario; //Cargar igual que lo de la foto
    
    public Empleado() {
    }

    public Empleado(int id, int cedula, String nombre, String apellidoUno, String apellidoDos, String telefono, char genero, char estadoCivil, String direccion, boolean activo) {
        this.id = id;
        this.cedula = cedula;
        this.nombre = nombre;
        this.apellidoUno = apellidoUno;
        this.apellidoDos = apellidoDos;
        this.telefono = telefono;
        this.genero = genero;
        this.estadoCivil = estadoCivil;
        this.direccion = direccion;
        this.activo = activo;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoUno() {
        return apellidoUno;
    }

    public void setApellidoUno(String apellidoUno) {
        this.apellidoUno = apellidoUno;
    }

    public String getApellidoDos() {
        return apellidoDos;
    }

    public void setApellidoDos(String apellidoDos) {
        this.apellidoDos = apellidoDos;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public char getGenero() {
        return genero;
    }

    public String getGeneroTxT() {
        switch (genero) {
            case 'M':
                return "Masc.";
            default:
                return "Femen.";
        }
    }

    public void setGenero(char genero) {
        this.genero = genero;
    }

    public char getEstadoCivil() {
        return estadoCivil;
    }

    public String getEstadoCivilTxT() {
        switch (estadoCivil) {
            case 'U':
                return "Unión Libre";
            case 'D':
                return "Divorciado/a";
            case 'C':
                return "Casado/a";
            case 'V':
                return "Viudo/a";
            default:
                return "Soltero/a";
        }
    }

    public void setEstadoCivil(char estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getNomApe() {
        return String.format("%s %s %s", nombre, apellidoUno, apellidoDos);
    }

    public Contrato getContrato() {
        return contrato;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public String toString() {
        return "Empleado{" + "id=" + id + ", cedula=" + cedula + ", nombre=" + nombre + ", apellidoUno=" + apellidoUno + ", apellidoDos=" + apellidoDos + ", telefono=" + telefono + ", genero=" + genero + ", estadoCivil=" + estadoCivil + ", direccion=" + direccion + ", activo=" + activo + ", contrato=" + contrato + ", usuario=" + usuario + '}';
    }  

}
