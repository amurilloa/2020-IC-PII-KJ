CREATE TABLE rrhh.usuarios
(
    id serial primary key,
    usuario text NOT NULL,
	correo text NOT NULL UNIQUE, 
    contrasena text NOT NULL,
    activo boolean DEFAULT true,
    CONSTRAINT unq_usuarios_usuario UNIQUE (usuario)
);
CREATE TABLE rrhh.empleados(
	id serial primary key,
	cedula text not null unique,
	nombre text not null,
	apellido_uno text not null,
	apellido_dos text,
	telefono text not null,
	genero char default 'M',
	estado_civil char default 'S',
	direccion text, 
	activo boolean default false
);

select * from rrhh.empleados
insert into rrhh.empleados(cedula, nombre, apellido_uno, apellido_dos, telefono, genero, estado_civil, direccion, activo) 
values (206470762, 'Allan', 'Murillo', 'Alfaro', '8526-2638', 'M','C', 'Chachagua', true)

select id, cedula, nombre, apellido_uno, apellido_dos, telefono, genero, estado_civil, direccion, activo from rrhh.empleados 
where lower(cedula) like lower('2063%') or lower(apellido_uno) like lower('2063%') order by apellido_uno, nombre

create table rrhh.imagenes(
    id serial primary key, 
    id_usuario int not null, 
    imagen bytea,
    constraint fk_ima_usu foreign key(id_usuario) references rrhh.usuarios(id)
)

create table rrhh.areas (
    id serial primary key,
    codigo text not null unique, 
    nombre text not null unique,
    activo boolean default true
);


create table rrhh.puestos (
    id serial primary key,
    id_area int not null,
    codigo text not null, 
    nombre text not null,
    salario numeric default 0,
    activo boolean default true,
    constraint fk_pue_are foreign key(id_area) references rrhh.areas(id), 
    constraint unq_codare unique(codigo, id_area)
)

alter table rrhh.puestos drop constraint unq_codnom
alter table rrhh.puestos add constraint unq_codare unique(codigo, id_area)

-- drop table rrhh.puestos
-- insert into rrhh.areas (codigo, nombre, activo) values('CONT','Contabilidad', true)
-- insert into rrhh.puestos(id_area, codigo, nombre,salario) values(3,'CO-GTE', 'Gerente', 800000)
-- select  * from rrhh.puestos
delete from rrhh.puestos where id = 3
create table rrhh.contratos(
    id serial primary key,
    id_empleado int not null, 
    id_puesto int not null, 
    fecha date not null, 
    salario numeric default 0, 
    activo boolean default true,
    constraint fk_con_emp foreign key(id_empleado) references rrhh.empleados(id), 
    constraint fk_con_pue foreign key(id_puesto) references rrhh.puestos(id)
)

-- insert into rrhh.contratos(id_empleado, id_puesto, fecha, salario) 
-- values(1, 1, '2018-01-01', 600000)
-- 
-- select * from rrhh.areas


alter table rrhh.usuarios add column id_empleado int
alter table rrhh.usuarios add constraint fk_usu_emp foreign key(id_empleado) references  rrhh.empleados(id)

select * from rrhh.usuarios

select * from rrhh.empleados
update rrhh.usuarios set id_empleado = 1 where id =1

update rrhh.usuarios set id_empleado = null where id_empleado = 1

select id, id_empleado, id_puesto, fecha, salario, activo from rrhh.contratos where id_empleado = 1 order by fecha desc limit 1


--drop table rrhh.vacaciones
create table rrhh.vacaciones(
    id serial primary key, 
    id_empleado int not null, 
    motivo text, 
    fecha_ini date not null,
    fecha_fin date not null,
    dias int default 0,
    observacion text default '', 
    estado char default 'S', 
    constraint fk_vac_emp foreign key(id_empleado) references rrhh.empleados(id), 
    constraint chk_vac_est check(estado in('S','R','A'))
)

--Insertar una solicitud de vacaciones --> Empleado 
insert into rrhh.vacaciones(id_empleado, motivo, fecha_ini, fecha_fin, estado) 
values(1, 'Personal', '2020-04-20', '2020-04-24','S')

select * from rrhh.vacaciones
--Aprobar o rechazar la solicitud
update rrhh.vacaciones set motivo=?, fecha_ini=?, fecha_fin=?, dias = ?, observacion=?, estado = ? where id = ?


select * from rrhh.usuarios

select u.*, (a.id=1) as admin
from 
    rrhh.usuarios u 
inner join rrhh.contratos c on u.id_empleado = c.id_empleado
inner join rrhh.puestos p on c.id_puesto = p.id
inner join rrhh.areas a on p.id_area = a.id
where u.usuario = 'amurillo' and u.contrasena ='25d55ad283aa400af464c76d713c07ad'
order by c.fecha desc limit 1

select * from rrhh.areas
select * from rrhh.puestos
select * from rrhh.contratos where id_empleado = 1
update rrhh.usuarios set activo = true where id = 2


select id, id_empleado, motivo, fecha_ini, fecha_fin, dias, observacion, estado from rrhh.vacaciones v 
where id_empleado = 1 order by fecha_ini

select e.id, e.cedula, e.nombre, e.apellido_uno, e.apellido_dos, e.telefono, e.genero, 
    e.estado_civil, e.direccion, e.activo from rrhh.empleados e 
inner join rrhh.usuarios u on e.id = u.id_empleado 
where u.id = ?


update rrhh.contratos set fecha ='2019-04-14' where id = 4
select  * from rrhh.contratos
