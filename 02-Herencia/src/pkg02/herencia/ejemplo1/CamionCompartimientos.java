/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg02.herencia.ejemplo1;

/**
 *
 * @author ALLAN
 */
public class CamionCompartimientos extends Camion {

    public int compartimientos;

    public int getCompartimientos() {
        return compartimientos;
    }

    public void setCompartimientos(int compartimientos) {
        this.compartimientos = compartimientos;
    }

    public double cargaCompartimiento() {
        return cantToneladas / compartimientos;
    }

    public String getDescripcion() {
        return marca + " " + compartimientos;
    }

}
