/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg02.herencia.ejemplo1;

/**
 *
 * @author ALLAN
 */
public class Main {

    public static void main(String[] args) {
        Autobus a = new Autobus();
        a.setPlaca("BHX-629");
        a.setMarca("Marco Polo");
        a.setCantAsientos(70);

        System.out.println(a);
        
        Camion c = new Camion();
        c.setPlaca("BHX-629");
        c.setMarca("CAT");
        c.setCantToneladas(2000);
        System.out.println(c);
        
        CamionCompartimientos cc = new CamionCompartimientos();
        cc.setPlaca("BHX-623");
        cc.setMarca("CATERPILLAR");
        cc.setCantToneladas(5000);
        cc.setCompartimientos(3);
        System.out.println(cc.cargaCompartimiento());
        System.out.println(cc.getDescripcion());

    }

}
