/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg02.herencia;

/**
 *
 * @author ALLAN
 */
public class Alumno extends Persona {

    public String carne;

    public Alumno() {
    }

    public Alumno(String carne, String cedula, String nombre) {
        super(cedula, nombre);
        this.carne = carne;
    }

    public String getCarne() {
        return carne;
    }

    public void setCarne(String carne) {
        this.carne = carne;
    }

    public String quienSoy() {
        return cedula + ": " + nombre + " - " + carne;
    }
}
