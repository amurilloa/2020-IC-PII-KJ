/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg02.herencia.videojuego;

/**
 *
 * @author ALLAN
 */
public class Guerrero extends Personaje {

    private String arma;

    public Guerrero(String nombre, String arma, int energia) {
        super(nombre, energia);
        this.arma = arma;
    }

    public String combatir(int cantidad) {
        //setEnergia(getEnergia()-cantidad);
        alimentarse(-cantidad);
        return arma + " " + cantidad;
    }

    public String getArma() {
        return arma;
    }

    public void setArma(String arma) {
        this.arma = arma;
    }

    @Override
    public String toString() {
        return "Guerrero{" + "arma=" + arma + " " + super.toString() + '}';
    }

}
