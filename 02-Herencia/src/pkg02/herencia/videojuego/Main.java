/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg02.herencia.videojuego;

/**
 *
 * @author ALLAN
 */
public class Main {

    public static void main(String[] args) {
        Guerrero g1 = new Guerrero("Axe", "Hacha", 100);
        g1.combatir(15);
        System.out.println(g1);

        Personaje m1 = new Mago("Quemar", "Harry Potter");
//        m1.encantar();
        ((Mago) m1).encantar();
        ((Mago) m1).encantar();
        System.out.println(m1);

        Personaje[] personajes = new Personaje[10];
        personajes[0] = g1;
        personajes[1] = m1;
//        personajes[2] = new Personaje("AllanBot", 1);
        personajes[3] = m1;
        personajes[4] = m1;
        personajes[5] = m1;

        System.out.println("------------>" + personajes[0]);
        ((Guerrero) personajes[0]).combatir(12);
        System.out.println("------------>" + personajes[0]);

        int canMag = 0;
        for (Personaje personaje : personajes) {

            if (personaje instanceof Mago) {
                ((Mago) personaje).encantar();
            } else if (personaje instanceof Guerrero) {
                ((Guerrero) personaje).combatir(15);
            }

            if (personaje instanceof Mago) {
                System.out.println("Soy un Mago" + personaje);
                canMag++;
            } else if (personaje instanceof Guerrero) {
                System.out.println("Soy un Guerrero" + personaje);
            } else if (personaje instanceof Personaje) {
                System.out.println("Soy un personaje");
            } else {
                System.out.println("null");
            }
        }
        
        System.out.println("Hay " + canMag + " magos");
        Mago p = new Mago("Quemar","Mago 2");
        p.setEnergia(12);
    }
}
