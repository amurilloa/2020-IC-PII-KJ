/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg02.herencia.videojuego;

/**
 *
 * @author ALLAN
 */
public class Mago extends Personaje {

    private String poder;

    public Mago(String poder, String nombre) {
        super(nombre, 100);
        this.poder = poder;
    }

    public String encantar() {
        alimentarse(-2);
        return poder;
    }

    public String getPoder() {
        return poder;
    }

    public void setPoder(String poder) {
        this.poder = poder;
    }

    @Override
    public void alimentarse(int cantidad) {
//        setEnergia(getEnergia() + cantidad);
        energia += cantidad;//Protected 
    }

    @Override
    public String toString() {
        return "Mago{" + "poder=" + poder + " " + super.toString() + '}';
    }
}
