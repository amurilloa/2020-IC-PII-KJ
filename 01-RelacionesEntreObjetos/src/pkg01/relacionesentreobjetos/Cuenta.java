/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg01.relacionesentreobjetos;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.LinkedList;

/**
 *
 * @author ALLAN
 */
public class Cuenta {

    private long numero;
    private Cliente titular;
    private float saldo;
    private float interesAnual;
    private LinkedList<Movimiento> movimientos;

    public Cuenta() {
        movimientos = new LinkedList<>();
    }

    public Cuenta(long numero, Cliente titular, float interesAnual) {
        this.numero = numero;
        this.titular = titular;
        this.interesAnual = interesAnual;
        movimientos = new LinkedList<>();
    }

    /**
     * 
     * @param cantidad 
     */
    public void ingreso(int cantidad) {
        saldo += Math.abs(cantidad);
        regisgra(LocalDate.now(), 'I', cantidad, saldo);
    }

    /**
     * 
     * @param cantidad 
     */
    public void reintegro(int cantidad) {
        saldo -= Math.abs(cantidad);
        regisgra(LocalDate.now(), 'R', cantidad, saldo);
    }

    /**
     * 
     * @param fecha
     * @param tipo
     * @param cantidad
     * @param saldo 
     */
    private void regisgra(LocalDate fecha, char tipo, int cantidad, float saldo) {
        movimientos.add(new Movimiento(fecha, tipo, cantidad, saldo));
    }

    public void ingresoInteresMensual() {
    }

    public boolean enRojos() {
        return saldo < 0;
    }

    public int leerSaldo() {
        return (int) saldo;
    }

    public void salvar() {
    }

    public long getNumero() {
        return numero;
    }

    public void setNumero(long numero) {
        this.numero = numero;
    }

    public Cliente getTitular() {
        return titular;
    }

    public void setTitular(Cliente titular) {
        this.titular = titular;
    }

    @Override
    public String toString() {
        String txt = "Cuenta: " + numero + "\nTitular: " + titular.nombreCompleto()
                + "\nDirección: " + titular.direccionCompleta() + "\nInterés Anual: "
                + interesAnual + "\nMovimientos: " + Arrays.toString(movimientos.toArray());
        return txt;
    }

}
