/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg01.relacionesentreobjetos;

import java.time.LocalDate;

/**
 *
 * @author ALLAN
 */
public class RelacionesEntreObjetos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Cliente cli = new Cliente("Allan", "Murillo Alfaro",
                "50m este de la Escuela Procopio Gamboa", "Chachagua",
                LocalDate.parse("1988-06-28"));

        
        Cliente cli2 = new Cliente("Roberto", "Murillo Alfaro",
                "50m este de la Escuela", "El Bosque",
                LocalDate.parse("1987-03-03"));
        
        Cuenta c1 = new Cuenta(123146654L, cli, 0.001F);
        c1.ingreso(5000);
        c1.ingreso(200000);
        c1.reintegro(100000);
        c1.reintegro(25000);
        c1.reintegro(4000);
        Cuenta c2 = new Cuenta(123146653L, null, 0.003F);
        c2.ingreso(1000);
        c2.ingreso(2000);
        c2.setTitular(cli2);
        System.out.println(c1);
        System.out.println(c2);
    }
    
}
