/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg03.delincuentes;

/**
 *
 * @author ALLAN
 */
public class Foto {

    private String ruta;
    private String tipo;

    public Foto() {
    }

    public Foto(String ruta, String tipo) {
        this.ruta = ruta;
        this.tipo = tipo;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "Foto{" + "ruta=" + ruta + ", tipo=" + tipo + '}';
    }
}
