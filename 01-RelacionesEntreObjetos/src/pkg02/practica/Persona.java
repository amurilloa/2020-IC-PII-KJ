/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg02.practica;

/**
 *
 * @author ALLAN
 */
public class Persona {

    private String nombre;
    private Corazon corazon;
    private Coche coche;

    public Persona(String nombre) {
        corazon = new Corazon();
        this.nombre = nombre;
    }

    public void asignaCoche(Coche coche) {
        this.coche = coche;
    }
    
    public void viaja() {
    }

    public void emociona() {
        corazon.cambiaRitmo(corazon.leerRitmo() + 30);
    }

    public void tranquiliza() {
        corazon.cambiaRitmo(corazon.leerRitmo() - 30);
    }

    @Override
    public String toString() {
        return "Persona{" + "nombre=" + nombre + ", corazon=" + corazon + ", coche=" + coche + '}';
    }
    
    
}
