/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg02.practica;

/**
 *
 * @author ALLAN
 */
public class Corazon {

    private int ritmo;

    public Corazon() {
        ritmo = 85;
    }

    public void cambiaRitmo(int ritmo) {
        this.ritmo = ritmo;
    }

    public int leerRitmo() {
        return ritmo;
    }

    @Override
    public String toString() {
        return "Corazon{" + "ritmo=" + ritmo + '}';
    }
    
    

}
