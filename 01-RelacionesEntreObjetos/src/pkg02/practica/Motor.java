/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg02.practica;

/**
 *
 * @author ALLAN
 */
public class Motor {

    private int rpm;
    private boolean activo;

    public void cambiaRPM(int rpm) {
        this.rpm = rpm;
    }

    public int leerRPM() {
        return rpm;
    }

    public boolean estaActivo() {
        return activo;
    }

    public void activa() {
        this.activo = true;
    }

    public void desactiva() {
        this.activo = false;
    }

    @Override
    public String toString() {
        return "Motor{" + "rpm=" + rpm + ", activo=" + activo + '}';
    }

    
}
