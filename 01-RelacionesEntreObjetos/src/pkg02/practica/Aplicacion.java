/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg02.practica;

/**
 *
 * @author ALLAN
 */
public class Aplicacion {

    public static void main(String[] args) {
        Motor m = new Motor();
        Coche c = new Coche(m);
        Persona p = new Persona("Allan Murillo Alfaro");
        p.asignaCoche(c);
        c.asignaConductor(p);
        System.out.println(p);
        //p.tranquiliza();
        p.emociona();

        System.out.println(m);
        System.out.println(c);
        System.out.println(p);
    }
}
