/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg02.practica;

/**
 *
 * @author ALLAN
 */
public class Coche {

    private Motor motor;
    private Persona conductor;

    public Coche(Motor motor) {
        this.motor = motor;
    }

    public void asignaConductor(Persona conductor) {
        this.conductor = conductor;
    }

    public void enciende() {
        if (motor != null && !motor.estaActivo()) {
            motor.activa();
            motor.cambiaRPM(1000);
        }
    }

    public void apaga() {
        if (motor != null && motor.estaActivo()) {
            motor.desactiva();
            motor.cambiaRPM(0);
        }
    }

    public void acelera() {

        if (motor != null && motor.estaActivo()) {
            motor.cambiaRPM(motor.leerRPM() + 500);
        }
    }

    public void frena() {

        if (motor != null && motor.estaActivo()) {
            motor.cambiaRPM(motor.leerRPM() - 500);
        }
    }

    public boolean estaEncendido() {
        if (motor != null) {
            return motor.estaActivo();
        }
        return false;
    }

    @Override
    public String toString() {
        return "Coche{" + "motor=" + motor + ", conductor="  + '}';
    }
    
    
}
