/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiendamusical;

import java.util.LinkedList;

/**
 *
 * @author ALLAN
 */
public class Logica {

    private LinkedList<ProductoMusical> productos;

    public Logica() {
        productos = new LinkedList<>();
        datosPrueba();
    }

    public boolean registrar(ProductoMusical pro) {
        for (ProductoMusical p : productos) {
            if (p.getCodigo().equals(pro.getCodigo())) {
                return false;
            }
        }
        productos.add(pro);
        return true;
    }

    public LinkedList<ProductoMusical> getProductos() {
        return productos;
    }

    public String getInfo(ProductoMusical pro) {
        String txt = "Categoría: %s\n"
                + "Código: %s\n"
                + "Autor: %s\n"
                + "Sello: %s\n";

        String cod = pro.getCodigo();
        String aut = pro.getAutor();
        String sel = pro.getSello();

        if (pro instanceof DiscoCompacto) {
            String cat = "CD";
            String tip = ((DiscoCompacto) pro).getTipo();
            String form = ((DiscoCompacto) pro).getFormato();
            String tam = ((DiscoCompacto) pro).getTamano();

            txt += "Tipo: %s\n"
                    + "Formato: %s\n"
                    + "Tamaño: %s";
            return String.format(txt, cat, cod, aut, sel, tip, form, tam);
        } else if (pro instanceof DiscoVersatilDigital) {

            DiscoVersatilDigital dvd = (DiscoVersatilDigital) pro;
            String cat = "DVD";
            String tip = dvd.getTipo();
            String form = dvd.getFormato();
            txt += "Tipo: %s\n"
                    + "Formato: %s";

            return String.format(txt, cat, cod, aut, sel, tip, form);

        } else if (pro instanceof DiscoVinilo) {
            String cat = "Disco Vinilo";
            String tam = ((DiscoVinilo) pro).getTamano();
            int rpm = ((DiscoVinilo) pro).getRpm();

            txt += "Tamaño: %s\n"
                    + "RPM: %d";
            return String.format(txt, cat, cod, aut, sel, tam, rpm);
        }

        return String.format(txt, "Otro", cod, aut, sel);
    }

    private void datosPrueba() {
        registrar(new DiscoCompacto("CD-ROM", "mp3", "12cm", "PM-001", "Universal MusicLatinEntertainment", "Britney Spears"));
        registrar(new DiscoCompacto("CD-ROM", "mp3", "8cm", "PM-002", "Universal MusicLatinEntertainment", "Cristina Aguilera"));
        registrar(new DiscoVinilo("12cm", 33, "PM-003", " Sony Music Entertainment", "Santana"));
        registrar(new DiscoVersatilDigital("DVD-RW", "mkv", "DBZ-001", "Toei Animation", "Akira Toriyama"));
    }

//    public boolean eliminar(ProductoMusical sel) {
//        for (int i = 0; i < productos.size(); i++) {
//            if (productos.get(i).getCodigo().equals(sel.getCodigo())) {
//                productos.remove(i);
//                return true;
//            }
//        }
//        return false;
//    }
    public boolean eliminar(ProductoMusical sel) {
        return productos.remove(sel);
    }

    public boolean editar(ProductoMusical nuevo, ProductoMusical viejo) {
        for (int i = 0; i < productos.size(); i++) {
            if (productos.get(i).equals(viejo)) {
                productos.remove(i);
                productos.add(i, nuevo);
                return true;
            }
        }
        return false;
    }
}
