/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiendamusical;

/**
 *
 * @author ALLAN
 */
public class DiscoVersatilDigital extends ProductoMusical {

    private String tipo;
    private String formato;

    public DiscoVersatilDigital() {
    }

    public DiscoVersatilDigital(String tipo, String formato, String codigo, String sello, String autor) {
        super(codigo, sello, autor);
        this.tipo = tipo;
        this.formato = formato;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getFormato() {
        return formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    @Override
    public String toString() {
        return codigo + " - " + autor + " (" + sello + ")";
    }
}

/*
    DVD-ROM: solo lectura, manufacturado con prensa.
    DVD-R y DVD+R: grabable una sola vez. ...
    DVD-RW y DVD+RW: regrabable.
    DVD-RAM: regrabable de acceso aleatorio. ...
    DVD+R DL: grabable una sola vez de doble capa.
    El DVD-ROM almacena desde 4,7 GB hasta 17 GB.
 */
