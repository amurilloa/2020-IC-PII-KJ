/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiendamusical;

/**
 *
 * @author amurilloa
 */
public class Persona {

    private String ced;
    private String nombre;
    private String apellido;

    public Persona() {
    }

    public Persona(String ced, String nombre, String apellido) {
        this.ced = ced;
        this.nombre = nombre;
        this.apellido = apellido;
    }

    public String getCed() {
        return ced;
    }

    public void setCed(String ced) {
        this.ced = ced;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getData() {
        return ced + "," + nombre + "," + apellido;
    }

    @Override
    public String toString() {
        return "Persona{" + "ced=" + ced + ", nombre=" + nombre + ", apellido=" + apellido + '}';
    }

}
