/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiendamusical;

import java.util.LinkedList;

/**
 *
 * @author amurilloa
 */
public class Main {

    public static void main(String[] args) {
        ManejoArchivos ma = new ManejoArchivos();
//        Persona p = new Persona("7", "Moserrath", "Murillo");
//        ma.escribir("datos.txt", datos + p.getData());

        LinkedList<Persona> personas = new LinkedList<>();
        String datos = ma.leer("datos.txt");
        String[] pers = datos.split("\n");
        for (String per : pers) {
            String[] camp = per.split(",");
            personas.add(new Persona(camp[0], camp[1], camp[2]));
        }

        for (Persona persona : personas) {
            System.out.println(persona);
        }

    }
}
