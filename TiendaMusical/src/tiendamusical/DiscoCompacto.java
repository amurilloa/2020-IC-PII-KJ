/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiendamusical;

/**
 *
 * @author ALLAN
 */
public class DiscoCompacto extends ProductoMusical {

    private String tipo;
    private String formato;
    private String tamano;

    public DiscoCompacto() {
    }

    public DiscoCompacto(String tipo, String formato, String tamano, String codigo, String sello, String autor) {
        super(codigo, sello, autor);
        this.tipo = tipo;
        this.formato = formato;
        this.tamano = tamano;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getFormato() {
        return formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public String getTamano() {
        return tamano;
    }

    public void setTamano(String tamano) {
        this.tamano = tamano;
    }

    @Override
    public String toString() {
        return codigo + " - " + autor + " (" + sello + ")";
    }

}


/*
    Solo lectura: CD-ROM (Compact Disc - Read Only Memory).
    Grabable: CD-R (Compact Disc - Recordable).
    Regrabable: CD-RW (Compact Disc - Re-Writable).
    De audio: CD-DA (Compact Disc - Digital Audio).
 */
