/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiendamusical;

/**
 *
 * @author ALLAN
 */
public class DiscoVinilo extends ProductoMusical {

    private String tamano;
    private int rpm;

    public DiscoVinilo() {
    }

    public DiscoVinilo(String tamano, int rpm, String codigo, String sello, String autor) {
        super(codigo, sello, autor);
        this.tamano = tamano;
        this.rpm = rpm;
    }

    public String getTamano() {
        return tamano;
    }

    public void setTamano(String tamano) {
        this.tamano = tamano;
    }

    public int getRpm() {
        return rpm;
    }

    public void setRpm(int rpm) {
        this.rpm = rpm;
    }

    @Override
    public String toString() {
        return codigo + " - " + autor + " (" + sello + ")";
    }

}
